package com.liu;

/**
 * @author liucong
 * @date 2021/2/4 - 21:53
 *
 * 有动态缩容操作
 */
public class ArrayList2<E> extends AbstractList<E> {

    /**
     * 所有的元素
     */
    private Object[] elements;

    private static final int DEFAULT_CAPACITY=10;

    public ArrayList2(int capacity){
        capacity = capacity<DEFAULT_CAPACITY ? DEFAULT_CAPACITY : capacity;
        elements=new Object[capacity];
    }

    public ArrayList2(){
        this(DEFAULT_CAPACITY);
    }

    /**
     * 清除所有的元素
     */
    public void clear(){
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }
        size=0;

        if(elements!=null && elements.length>DEFAULT_CAPACITY){
            elements=new Object[DEFAULT_CAPACITY];
        }
    }

    /**
     * 获取index位置的元素
     */
    public E get(int index){
        rangeCheck(index);
        return (E) elements[index]; //O(1)
    }

    /**
     * 设置index位置的元素
     * @param index
     * @param element
     * @return 原来的元素
     */
    public E set(int index,E element){ //O(1)
        rangeCheck(index);
        Object old=elements[index];
        elements[index]=element;
        return (E) old;
    }

    /**
     * 在index位置插入一个元素
     */
    public void add(int index,E element){
        /*
        最好：O(1)
        最坏：O(n)
        平均：O(n)  (1+2+...+n)/n
         */
        rangeCheckForAdd(index);
        ensureCapacity(size+1);
        for (int i = size; i >index; i--) {
            elements[i]=elements[i-1];
        }
        elements[index]=element;
        size++;
    }

    /**
     * 删除index位置的元素
     */
    public E remove(int index){
        /*
        最好：O(1)
        最坏：O(n)
        平均：O(n)
         */
        rangeCheck(index);
        Object old=elements[index];
        for (int i = index+1; i < size; i++) {
            elements[i-1]=elements[i];
        }
        size--;
        elements[size]=null;
        trim();
        return (E) old;
    }

    /**
     * 查看元素的位置
     */
    public int indexOf(E element){
        if(element==null){
            for (int i = 0; i < size; i++) {
                if(elements[i]==null)
                    return i;
            }
        }else{
            for (int i = 0; i < size; i++) {
                if(element.equals(elements[i]))
                    return i;
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * 确保capacity足够
     * @param capacity
     */
    private void ensureCapacity(int capacity){
        int oldCapacity = elements.length;
        if(capacity<=oldCapacity) return;

        //新容量为旧容量的1.5倍
        int newCapacity = oldCapacity + (oldCapacity>>1);
        Object[] newElements = new Object[newCapacity];
        for (int i = 0; i < size; i++) {
            newElements[i]=elements[i];
        }
        elements=newElements;

        System.out.println(oldCapacity+" 扩容为："+newCapacity);
    }

    private void trim(){
        int oldCapacity = elements.length;
        int newCapacity = oldCapacity>>1;
        if(size >= newCapacity || oldCapacity <= DEFAULT_CAPACITY)
            return;

        //缩容（剩余空间还很多）
        Object[] newElements = new Object[newCapacity];
        for (int i = 0; i < size; i++) {
            newElements[i]=elements[i];
        }
        elements=newElements;
        System.out.println(oldCapacity+" 缩容为："+newCapacity);
    }

    @Override
    public String toString() {
        // size=3, [99, 88, 77]
        StringBuilder string = new StringBuilder();
        string.append("size=").append(size).append(", ").append("[");
        for (int i = 0; i < size; i++) {
            if(i!=0)
                string.append(", ");
            string.append(elements[i]);
//            if(i!=size-1)
//                string.append(", ");
        }
        string.append("]");
        return string.toString();
    }
}
