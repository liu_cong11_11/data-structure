package com.liu;

/**
 * @author liucong
 * @date 2021/2/15 - 20:58
 */
public abstract class AbstractList<E> implements List<E> {
    /**
     * 元素的数量
     */
    protected int size;

    /**
     * 元素的数量
     */
    public int size(){
        return size;
    }

    /**
     * 是否为空
     */
    public boolean isEmpty(){
        return size==0;
    }

    /**
     * 是否包含某个元素
     */
    public boolean contains(E element){
        return indexOf(element)!=ELEMENT_NOT_FOUND;
    }

    /**
     * 添加元素到尾部
     */
    public void add(E element){
        /*
        最好：O(1) 绝大部分
        最坏：O(n)
        平均：O(1)
        均摊：O(1)
         */
        add(size,element);
    }

    protected void indexOutOfBounds(int index){
        throw new IndexOutOfBoundsException("index:"+index+", size:"+size);
    }

    protected void rangeCheck(int index){
        if(index<0||index>=size)
            indexOutOfBounds(index);
    }

    protected void rangeCheckForAdd(int index){
        if(index<0||index>size)
            indexOutOfBounds(index);
    }
}
