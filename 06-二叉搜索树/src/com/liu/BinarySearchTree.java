package com.liu;

import com.liu.printer.BinaryTreeInfo;
import sun.reflect.generics.visitor.Visitor;

import javax.sound.midi.Soundbank;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author liucong
 * @date 2021/2/26 - 10:18
 */
public class BinarySearchTree<E> implements BinaryTreeInfo {
    private int size;
    private Node<E> root;
    private Comparator<E> comparator;

    public BinarySearchTree(){
        this(null);
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void clear() {

    }

    public void add(E element) {
        elementNotNullCheck(element);

        // 添加第一个节点
        if (root == null) {
            root = new Node<>(element, null);
            size++;
            return;
        }

        // 添加的不是第一个节点
        //找到父节点
        Node<E> parent = root;
        Node<E> node = root;
        int cmp = 0;
        while (node != null) {
            cmp = compare(element, node.element);
            parent = node;

            if (cmp > 0) {
                node = node.right;
            } else if (cmp < 0) {
                node = node.left;
            } else { //相等
                node.element = element;
                return;
            }
        }

        //看看插入到父节点的哪个位置
        Node<E> newNode = new Node<>(element, parent);
        if (cmp > 0) {
            parent.right = newNode;
        } else {
            parent.left = newNode;
        }
        size++;
    }

    public void remove(E element) {

    }

    public boolean contains(E element) {
        return false;
    }

    /**
     * 前序遍历
     */
    public void preorderTraversal(){
        preorderTraversal(root);
    }

    private void preorderTraversal(Node<E> node){
        if(node == null) return;

        System.out.println(node.element);
        preorderTraversal(node.left);
        preorderTraversal(node.right);
    }

    /**
     * 前序遍历
     * @param visitor 对遍历元素的处理方式
     */
    public void preorder(Visitor<E> visitor){
        preorder(root, visitor);
    }

    private void preorder(Node<E> node, Visitor<E> visitor) {
        if(node == null || visitor == null) return;

        visitor.visit(node.element);
        preorder(node.left, visitor);
        preorder(node.right, visitor);
    }

    /**
     * 中序遍历
     */
    public void inorderTraversal(){
        inorderTraversal(root);
    }

    private void inorderTraversal(Node<E> node){
        if(node == null) return;

        inorderTraversal(node.left);
        System.out.println(node.element);
        inorderTraversal(node.right);
    }

    public void inorder(Visitor<E> visitor){
        inorder(root, visitor);
    }

    private void inorder(Node<E> node, Visitor<E> visitor) {
        if(node == null || visitor == null) return;

        inorder(node.left, visitor);
        visitor.visit(node.element);
        inorder(node.right, visitor);
    }

    /**
     * 后序遍历
     */
    public void postorderTraversal(){
        postorderTraversal(root);
    }

    private void postorderTraversal(Node<E> node){
        if(node == null) return;

        postorderTraversal(node.left);
        postorderTraversal(node.right);
        System.out.println(node.element);
    }

    public void postorder(Visitor<E> visitor) {
        postorder(root, visitor);
    }

    private void postorder(Node<E> node, Visitor<E> visitor) {
        if(node == null || visitor == null) return;

        postorder(node.left, visitor);
        postorder(node.right, visitor);
        visitor.visit(node.element);
    }

    /**
     * 层序遍历
     */
    public void levelOrderTraversal(){
        if(root == null) return;

        Queue<Node<E>> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()){
            Node<E> node = queue.poll();
            System.out.println(node.element);

            if(node.left != null){
                queue.offer(node.left);
            }
            if(node.right != null){
                queue.offer(node.right);
            }
        }
    }

    /**
     * 层序遍历
     * @param visitor 对遍历元素的处理方式
     */
    public void levelOrder(Visitor<E> visitor) {
        if(root == null || visitor == null) return;

        Queue<Node<E>> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()){
            Node<E> node = queue.poll();
            visitor.visit(node.element);

            if(node.left != null){
                queue.offer(node.left);
            }
            if(node.right != null){
                queue.offer(node.right);
            }
        }
    }

    /**
     * root节点的高度
     * @return
     */
    public int height() {
        return height(root);
    }

    /**
     *node节点的高度
     * @return
     */
    private int height(Node<E> node) {
        if(node == null) return 0;
        return 1 + Math.max(height(node.left), height(node.right));
    }

    /**
     * root节点的高度(非递归)
     * @return
     */
    public int height2() {
        if(root == null) return 0;

        //树的高度
        int height = 0;
        //每一层的元素数量
        int levelCount = 1;
        Queue<Node<E>> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()){
            Node<E> node = queue.poll();
            levelCount--;

            if(node.left != null){
                queue.offer(node.left);
            }
            if(node.right != null){
                queue.offer(node.right);
            }

            if(levelCount == 0){ //即将访问下一层
                levelCount = queue.size();
                height++;
            }
        }
        return height;
    }

    public static interface Visitor<E> {
        void visit(E element);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        toString(root, sb, "");
        return sb.toString();
    }

    private void toString(Node<E> node, StringBuilder sb, String prefix) {
        if(node == null) return;

        toString(node.left, sb, prefix + "L---");
        sb.append(prefix).append(node.element).append("\n");
        toString(node.right, sb, prefix + "R---");
    }

    public boolean isComplete() {
        if(root == null) return false;

        Queue<Node<E>> queue = new LinkedList<>();
        queue.offer(root);

        boolean leaf = false;
        while (!queue.isEmpty()){
            Node<E> node = queue.poll();

            if(leaf && !node.isLeaf()) return false;

            if(node.hasTwoChildren()){
                queue.offer(node.left);
                queue.offer(node.right);
            } else if (node.left == null && node.right != null){
                return false;
            }else{
                //(node.left !=null && node.right == null ) || (node.left == null && node.right == null)
                //后面遍历的节点都必须是叶子节点
                leaf = true;
            }
        }
        return true;
    }

    /**
     * 前驱节点
     * @param node
     * @return
     */
    private Node<E> predecessor(Node<E> node) {
        if(node == null) return null;

        //前驱节点在左子树当中（node.left.right.right...）
        Node<E> p = node.left;
        if(p != null) {
            while(p.right != null){
                p=p.right;
            }
            return p;
        }

        //node.left == null ,从父节点、祖父节点中寻找前驱节点
        while(node.parent != null && node == node.parent.left){
            node = node.parent;
        }

        //node.parent == null
        //node == node.parent.right
        return node.parent;
    }

    /**
     * 后继节点
     * @param node
     * @return
     */
    private Node<E> successor(Node<E> node) {
        if(node == null) return null;

        //后继节点在右子树当中（node.right.left.left...）
        Node<E> p = node.right;
        if(p != null) {
            while(p.left != null){
                p=p.left;
            }
            return p;
        }

        //node.right == null ,从父节点、祖父节点中寻找后继节点
        while(node.parent != null && node == node.parent.right){
            node = node.parent;
        }

        //node.parent == null
        //node == node.parent.left
        return node.parent;
    }

    /**
     *
     * @param e1
     * @param e2
     * @return  返回值等于0，代表e1和e2相等；返回值大于0，代表e1大于e2；返回值小于0，代表e1小于e2。
     */
    private int compare(E e1, E e2){
        if(comparator != null){
            return comparator.compare(e1, e2);
        }
        // 没传比较器
        return ((Comparable<E>)e1).compareTo(e2);
    }

    private void elementNotNullCheck(E element) {
        if(element == null) {
            throw new IllegalArgumentException("element must not be null");
        }
    }

    private static class Node<E> {
        E element;
        Node<E> left;
        Node<E> right;
        Node<E> parent;

        public Node(E element, Node<E> parent) {
            this.element = element;
            this.parent = parent;
        }

        public boolean isLeaf() {
            return left == null && right == null;
        }

        public boolean hasTwoChildren() {
            return left != null && right != null;
        }
    }

    /**
     *
     * @return  根节点
     */
    @Override
    public Object root() {
        return root;
    }

    /**
     *
     * @param node
     * @return  传入节点的左子节点
     */
    @Override
    public Object left(Object node) {
        return ((Node<E>)node).left;
    }

    /**
     *
     * @param node
     * @return  传入节点的右子节点
     */
    @Override
    public Object right(Object node) {
        return ((Node<E>)node).right;
    }

    /**
     *
     * @param node
     * @return  节点信息
     */
    @Override
    public Object string(Object node) {
        Node<E> myNode = (Node<E>)node;
        String parentString = "null";
        if(myNode.parent != null){
            parentString = myNode.parent.element.toString();
        }
        return myNode.element + "_p(" + parentString + ")";
    }
}
