package com.liu;

import com.liu.file.Files;
import com.liu.printer.BinaryTreeInfo;
import com.liu.printer.BinaryTrees;

import java.util.Comparator;

/**
 * @author liucong
 * @date 2021/2/26 - 11:32
 */
public class Main {

    private static class PersonComparator implements Comparator<Person> {
        @Override
        public int compare(Person e1, Person e2) {
            return e1.getAge() - e2.getAge();
        }
    }

    static void test1(){
        Integer data[] = new Integer[]{
                7, 4, 9, 2, 5, 8, 11, 3
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }

        BinaryTrees.println(bst);
    }

    static void test2(){
        Integer data[] = new Integer[]{
                7, 4, 9, 2, 5, 8, 11, 3
        };

        BinarySearchTree<Person> bst1 = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst1.add(new Person(data[i]));
        }
        BinaryTrees.println(bst1);

        System.out.println();

        //匿名类
        BinarySearchTree<Person> bst2 = new BinarySearchTree<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o2.getAge() - o1.getAge();
            }
        });

        for (int i = 0; i < data.length; i++) {
            bst2.add(new Person(data[i]));
        }
        BinaryTrees.println(bst2);
    }

    static void test3(){
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < 30; i++) {
            bst.add((int)(Math.random()*100));
        }
        //BinaryTrees.print(bst);
        String str = BinaryTrees.printString(bst);
        str += "\n";
        Files.writeToFile("D:/1.txt", str, true);
    }

    static void test4(){
        BinaryTrees.println(new BinaryTreeInfo() {
            @Override
            public Object root() {
                return "A";
            }

            @Override
            public Object left(Object node) {
                if(node.equals("A")) return "B";
                if(node.equals("C")) return "D";
                return null;
            }

            @Override
            public Object right(Object node) {
                if(node.equals("A")) return "C";
                if(node.equals("C")) return "E";
                return null;
            }

            @Override
            public Object string(Object node) {
                return node;
            }
        });
    }

    static void test5(){
        BinarySearchTree<Person> bst = new BinarySearchTree<>();
        bst.add(new Person(10, "jack"));
        bst.add(new Person(12, "rose"));
        bst.add(new Person(6, "Tom"));

        bst.add(new Person(10, "Mark"));

        BinaryTrees.println(bst);
    }

    static void test6(){
        Integer data[] = new Integer[]{
                7, 4, 2, 1, 3, 5, 9, 8, 11, 10, 12
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }

        BinaryTrees.println(bst);
//        bst.levelOrderTraversal();
        bst.levelOrder(new BinarySearchTree.Visitor<Integer>() {
            @Override
            public void visit(Integer element) {
                System.out.print("_"+element+"_ ");
            }
        });
    }

    static void test7() {
        Integer data[] = new Integer[]{
                7, 4, 2, 1, 3, 5, 9, 8, 11, 10, 12
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }

        System.out.println(bst);
    }

    static void test8() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < 30; i++) {
            bst.add((int)(Math.random()*100));
        }

        BinaryTrees.println(bst);
        System.out.println(bst.height2());
    }

    static void test9() {
        Integer data[] = new Integer[]{
                7, 4, 9, 2, 5
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }

        BinaryTrees.println(bst);
        System.out.println(bst.isComplete());
    }

    public static void main(String[] args) {
//        test1();
//        test2();
//        test3();
//        test4();
//        test5();
//        test6();
//        test7();
//        test8();
        test9();
    }
}
