package com.liu;

import com.liu.list.LinkedList;
import com.liu.list.List;

/**
 * @author liucong
 * @date 2021/2/25 - 10:15
 *
 * 双端队列
 */
public class Deque<E> {
    private List<E> list=new LinkedList<>();

    public void clear(){
        list.clear();
    }

    public int size(){
        return list.size();
    }

    public boolean isEmpty(){
        return list.isEmpty();
    }

    /**
     * 尾部入队
     * @param element
     */
    public void enQueueRear(E element){
        list.add(element);
    }

    /**
     * 头部出队
     * @return
     */
    public E deQueueFront(){
        return list.remove(0);
    }

    /**
     * 头部入队
     * @param element
     */
    public void enQueueFront(E element){
        list.add(0,element);
    }

    /**
     * 尾部出队
     * @return
     */
    public E deQueueRear(){
        return list.remove(list.size()-1);
    }

    public E front(){
        return list.get(0);
    }

    public E rear(){
        return list.get(list.size()-1);
    }
}
