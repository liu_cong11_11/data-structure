package com.liu;

import com.liu.circle.CircleDeque;
import com.liu.circle.CircleQueue;

/**
 * @author liucong
 * @date 2021/2/24 - 20:47
 */
public class Main {
    public static void main(String[] args) {
//        testDeque();
//        testCircleQueue();
        testCircleDeque();
    }

    static void testCircleDeque(){
        CircleDeque<Integer> queue = new CircleDeque<>();
        for (int i = 0; i < 10; i++) {
            queue.enQueueFront(i+1);
            queue.enQueueRear(i+101);
        }
        //头 5 4 3 2 1 101 102 103 104 105 106 107 8 7 6 尾
        //头 8 7 6 5 4 3 2 1 101 102 103 104 105 106 107 108 109 110 null null 10 9 尾

        for (int i = 0; i < 3; i++) {
            queue.deQueueFront();
            queue.deQueueRear();
        }
        //头 null 7 6 5 4 3 2 1 101 102 103 104 105 106 107 null null null null null null null 尾

        queue.enQueueFront(11);
        queue.enQueueFront(12);
        //头 11 7 6 5 4 3 2 1 101 102 103 104 105 106 107 null null null null null null 12 尾
        System.out.println(queue);
        while (!queue.isEmpty()){
            System.out.println(queue.deQueueFront());
        }
    }

    static void testCircleQueue(){
        CircleQueue<Integer> circleQueue = new CircleQueue<>();
        for (int i = 0; i < 10; i++) {
            circleQueue.enQueue(i);
        }
        //0 1 2 3 4 5 6 7 8 9

        for (int i = 0; i < 5; i++) {
            circleQueue.deQueue();
        }
        //null null null null null 5 6 7 8 9

        for (int i = 15; i < 23; i++) {
            circleQueue.enQueue(i);
        }

        System.out.println(circleQueue);
        while (!circleQueue.isEmpty()){
            System.out.println(circleQueue.deQueue());
        }
    }

    static void testDeque(){
        Deque<Integer> deque = new Deque<>();
        deque.enQueueFront(11);
        deque.enQueueFront(22);
        deque.enQueueRear(33);
        deque.enQueueRear(44);
        //尾  44 33 11 22 头

        while (!deque.isEmpty()){
            System.out.println(deque.deQueueFront());
        }
    }

    static void testQueue(){
        Queue<Integer> queue = new Queue<>();
        queue.enQueue(11);
        queue.enQueue(22);
        queue.enQueue(33);
        queue.enQueue(44);

        while (!queue.isEmpty()){
            System.out.println(queue.deQueue());
        }
    }
}
