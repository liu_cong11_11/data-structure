package com.liu;

import com.liu.list.LinkedList;
import com.liu.list.List;

/**
 * @author liucong
 * @date 2021/2/24 - 20:47
 */
public class Queue<E> {
    private List<E> list=new LinkedList<>();

    public void clear(){
        list.clear();
    }

    public int size(){
        return list.size();
    }

    public boolean isEmpty(){
        return list.isEmpty();
    }

    /**
     * 入队
     * @param element
     */
    public void enQueue(E element){
        list.add(element);
    }

    /**
     * 出队
     * @return
     */
    public E deQueue(){
        return list.remove(0);
    }

    /**
     * 队首元素
     * @return
     */
    public E front(){
        return list.get(0);
    }
}
