package com.liu;

import com.liu.list.ArrayList;
import com.liu.list.List;

/**
 * @author liucong
 * @date 2021/2/23 - 12:34
 */
public class Stack<E> {
    private List<E> list=new ArrayList<>();

    public void clear(){
        list.clear();
    }

    public int size(){
        return list.size();
    }

    public boolean isEmpty(){
        return list.isEmpty();
    }

    public void push(E element){
        list.add(element);
    }

    public E pop(){
        return list.remove(list.size()-1);
    }

    public E top(){
        return list.get(list.size()-1);
    }
}
