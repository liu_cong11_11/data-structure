package com.liu;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author liucong
 * @date 2021/2/23 - 12:34
 */
public class Main {
    public static void main(String[] args) {
//        java.util.Stack<Integer> stack=new Stack<>();
        Stack<Integer> stack=new Stack<>();
        stack.push(11);
        stack.push(22);
        stack.push(33);
        stack.push(44);

        while (!stack.isEmpty()){
            System.out.println(stack.pop());
        }

    }
}
