package com.liu;

/**
 * @author liucong
 * @date 2021/2/2 - 20:01
 */
public class Test {
    /*
     * 0 1 1 2 3 5 8 13 . . .
     */

    public static void main(String[] args) {
//        System.out.println(fib1(70));
//        System.out.println(fib2(70));
        Times.test("fib1", new Times.Task() {
            @Override
            public void execute() {
                System.out.println(fib1(45));
            }
        });
        Times.test("fib2", new Times.Task() {
            @Override
            public void execute() {
                System.out.println(fib2(45));
            }
        });
    }

    public static int fib1(int n){
        if(n<=1)
            return n;
        return fib1(n-1)+fib1(n-2);
    }

    public static int fib2(int n){
        if(n<=1)
            return n;
        int first=0;
        int second=1;
        int sum=0;
        for(int i=2;i<=n;i++){
            sum=first+second;
            first=second;
            second=sum;
        }
        return sum;
    }
}
