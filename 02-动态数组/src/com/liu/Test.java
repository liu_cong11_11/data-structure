package com.liu;


/**
 * @author liucong
 * @date 2021/2/4 - 21:49
 */
public class Test {
    public static void main(String[] args) {
        ArrayList<Person> list=new ArrayList<>();
//        list.add(99);
//        list.add(88);
//        list.add(77);
//        list.add(66);
//        list.add(55);

        list.add(new Person(12,"Jack"));
        list.add(new Person(15,"Tom"));
        list.add(null);
        list.add(new Person(17,"Rose"));
        System.out.println(list.indexOf(null));

//        list.clear();

        //提醒JVM进行垃圾回收
//        System.gc();

        System.out.println(list);
    }
}
