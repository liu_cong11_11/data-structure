package com.liu;

/**
 * @author liucong
 * @date 2021/2/15 - 14:06
 */
public class Person {
    private int age;
    private String name;

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        System.out.println("Person-finalize");
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null)
            return false;
        if(obj instanceof Person){
            Person person = (Person) obj;
            return this.age==person.age && this.name.equals(person.name);
        }
        return false;
    }
}
